#include <stdio.h>
#include <math.h>
#include "xil_printf.h"
#include "xmytestpatterngenerator.h"
#include "xgpio.h"

#define __TRACE

int Status;

XGpio gpio_inst;
XMytestpatterngenerator tpg_inst;

void writeData(uint16_t* data, size_t len)
{
	// TODO: automate this
	XMytestpatterngenerator_Set_data_0_V(&tpg_inst, data[0]);
	XMytestpatterngenerator_Set_data_1_V(&tpg_inst, data[1]);
	XMytestpatterngenerator_Set_data_2_V(&tpg_inst, data[2]);
	XMytestpatterngenerator_Set_data_3_V(&tpg_inst, data[3]);
	XMytestpatterngenerator_Set_data_4_V(&tpg_inst, data[4]);
	XMytestpatterngenerator_Set_data_5_V(&tpg_inst, data[5]);
	XMytestpatterngenerator_Set_data_6_V(&tpg_inst, data[6]);
	XMytestpatterngenerator_Set_data_7_V(&tpg_inst, data[7]);
	XMytestpatterngenerator_Set_data_8_V(&tpg_inst, data[8]);
	XMytestpatterngenerator_Set_data_9_V(&tpg_inst, data[9]);
	XMytestpatterngenerator_Set_data_10_V(&tpg_inst, data[10]);
	XMytestpatterngenerator_Set_data_11_V(&tpg_inst, data[11]);
	XMytestpatterngenerator_Set_data_12_V(&tpg_inst, data[12]);
	XMytestpatterngenerator_Set_data_13_V(&tpg_inst, data[13]);
	XMytestpatterngenerator_Set_data_14_V(&tpg_inst, data[14]);
	XMytestpatterngenerator_Set_data_15_V(&tpg_inst, data[15]);
	XMytestpatterngenerator_Set_data_16_V(&tpg_inst, data[16]);
	XMytestpatterngenerator_Set_data_17_V(&tpg_inst, data[17]);
	XMytestpatterngenerator_Set_data_18_V(&tpg_inst, data[18]);
	XMytestpatterngenerator_Set_data_19_V(&tpg_inst, data[19]);
	XMytestpatterngenerator_Set_data_20_V(&tpg_inst, data[20]);
	XMytestpatterngenerator_Set_data_21_V(&tpg_inst, data[21]);
	XMytestpatterngenerator_Set_data_22_V(&tpg_inst, data[22]);
	XMytestpatterngenerator_Set_data_23_V(&tpg_inst, data[23]);
	XMytestpatterngenerator_Set_data_24_V(&tpg_inst, data[24]);
	XMytestpatterngenerator_Set_data_25_V(&tpg_inst, data[25]);
	XMytestpatterngenerator_Set_data_26_V(&tpg_inst, data[26]);
	XMytestpatterngenerator_Set_data_27_V(&tpg_inst, data[27]);
	XMytestpatterngenerator_Set_data_28_V(&tpg_inst, data[28]);
	XMytestpatterngenerator_Set_data_29_V(&tpg_inst, data[29]);
	XMytestpatterngenerator_Set_data_30_V(&tpg_inst, data[30]);
	XMytestpatterngenerator_Set_data_31_V(&tpg_inst, data[31]);
}

uint8_t parseGPIO(uint32_t data)
{
	uint8_t ret = 0x0;
	ret |= (data & 0x000080) >> 6;  // bit 7 (V10)
	ret |= (data & 0x000100) >> 5;  // bit 8 (V8)
	ret |= (data & 0x008000) >> 15; // bit 15 (U7)
	ret |= (data & 0x100000) >> 18; // bit 20 (U8)
	return ret;
}

void generateValues(uint16_t* data, uint16_t x0, uint16_t sx, uint16_t sy, uint8_t a, uint8_t b, uint8_t c)
{
	for(int x = x0, i = 0; i < 32; i += 2, x++)
	{
		data[i + 0] = sy * (a * pow(x, b) + c);
		data[i + 1] = x * sx;
#ifdef __TRACE
		xil_printf("val %d (%d, %d)\r\n", i, data[i + 0], data[i + 1]);
#endif // __TRACE
	}
}

void waitForNum(uint8_t num)
{
	for(;;)
	{
		uint32_t gpio_data = XGpio_DiscreteRead(&gpio_inst, 1);
		uint8_t tmpnum = parseGPIO(gpio_data);

		if(tmpnum == num)
			break;

		usleep(1000);
	}
}

uint8_t receiveB()
{
	uint8_t ret = 0x0;

	// wait for start
#ifdef __TRACE
    xil_printf("waiting for 0xF...\r\n");
#endif // __TRACE
	waitForNum(0xF);

	// wait 1s for next msg
	sleep(1);
	usleep(5000); // some extra wait time

	// read first 4 bit
	ret |= parseGPIO(XGpio_DiscreteRead(&gpio_inst, 1)) << 0;
#ifdef __TRACE
    xil_printf("readLow 0x%x\r\n", ret);
#endif // __TRACE

	// wait for 2nd 4 bit
	sleep(1);

	// read 2nd 4 bit
	ret |= parseGPIO(XGpio_DiscreteRead(&gpio_inst, 1)) << 4;
#ifdef __TRACE
    xil_printf("readHi 0x%x\r\n", ret);
#endif // __TRACE

	return ret;
}

int main() {
	// GPIO driver init
	Status = XGpio_Initialize(&gpio_inst, XPAR_AXI_GPIO_0_DEVICE_ID);
	if (Status != XST_SUCCESS) {
		xil_printf("GPIO configuration failed\r\n");
		return XST_FAILURE;
	}

	// Set the direction to input
	XGpio_SetDataDirection(&gpio_inst, 1, ~0x0);

	// test
	/*for(;;)
	{
		//XGpio_DiscreteWrite(&gpio_inst, 1, 0x8000);
		uint32_t gpio_data = XGpio_DiscreteRead(&gpio_inst, 1);
		uint8_t num = parseGPIO(gpio_data);
		xil_printf("GPIO Data: %d (0x%x)\r\n", num, gpio_data);
		sleep(1);
	}*/

	// receive params
	uint8_t a = receiveB();
	uint8_t b = receiveB();
	uint8_t c = receiveB();

	// generate data
	uint16_t data[32];
	generateValues(data, 0, 50, 1, a, b, c);
	/*for(int i = 0; i < 32; i += 2)
	{
		data[i + 0] = i * 20;
		data[i + 1] = i * 20;
	}*/

	// init
	Status = XMytestpatterngenerator_Initialize(&tpg_inst, XPAR_MYTESTPATTERNGENERAT_0_DEVICE_ID);
	if(Status != XST_SUCCESS){
		xil_printf("TPG configuration failed\r\n");
		return(XST_FAILURE);
	}

	writeData(data, 32);
	XMytestpatterngenerator_Set_cntl_V(&tpg_inst, 0x22);

	// start
	XMytestpatterngenerator_EnableAutoRestart(&tpg_inst);
	XMytestpatterngenerator_Start(&tpg_inst);
	xil_printf("TPG started!\r\n");

	while(1){}

	return 0;
}
