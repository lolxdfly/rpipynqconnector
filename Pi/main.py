#!/usr/bin/python
import RPi.GPIO as GPIO
from time import sleep
import time
import sys

# write a 4 bit number to GPIO ports
def writeNum(num):
    if num & 0x1:
        GPIO.output(11, GPIO.HIGH)
    else:
        GPIO.output(11, GPIO.LOW)
    if num & 0x2:
        GPIO.output(13, GPIO.HIGH)
    else:
        GPIO.output(13, GPIO.LOW)
    if num & 0x4:
        GPIO.output(15, GPIO.HIGH)
    else:
        GPIO.output(15, GPIO.LOW)
    if num & 0x8:
        GPIO.output(19, GPIO.HIGH)
    else:
        GPIO.output(19, GPIO.LOW)

# hold num for specific time
def holdNum(num, ms):
    end = time.time_ns() + ms * 1000000
    while time.time_ns() < end:
        writeNum(num)

# use simple protocol to write a whole byte
def writeB(num):
    print("writing 0xF ...")
    writeNum(0xF)
    sleep(1)
    print("writing", hex(num & 0xF), "...")
    holdNum((num & 0xF), 1000)
    print("writing", hex((num >> 4) & 0xF), "...")
    holdNum(((num >> 4) & 0xF), 1000)

# use board pin defs
GPIO.setmode(GPIO.BOARD)

# mark output pins
GPIO.setup(11, GPIO.OUT)
GPIO.setup(13, GPIO.OUT)
GPIO.setup(15, GPIO.OUT)
GPIO.setup(19, GPIO.OUT)

try:
    # hold all pins to 0 to prevent sending 0xF by fault
    writeNum(0)
    input("Press any key to start sending sequence...")
    
    # test
    #num = 0
    #while True:
    #    writeNum(num)
    #    print("writing", num, "...")
    #    num = num + 1
    #    sleep(1)

    if len(sys.argv) != 4:
        print("Usage: python3 main.py a b c")
        exit()

    a = int(sys.argv[1])
    b = int(sys.argv[2])
    c = int(sys.argv[3])

    if a > 255 or b > 255 or c > 255:
        print("max value 255")
        exit()

    # f(x) = a * x ^ b + c
    writeB(a)
    writeB(b)
    writeB(c)
except KeyboardInterrupt:
    print("Exit")
#except:
#    print("Error")
finally:
    # cleanup GPIO state
    GPIO.cleanup()

