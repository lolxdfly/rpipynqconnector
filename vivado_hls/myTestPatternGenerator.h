#ifndef _VPG_H_
#define _VPG_H_

#include "/opt/Xilinx/Vivado/2020.1/include/gmp.h"
#include "hls_stream.h"
#include "ap_int.h"
#include "common/xf_axi_sdata.hpp"

#define WIDTH   1280
#define HEIGHT 	720

typedef hls::stream<xf::cv::ap_axiu<24,1,1,1> > AXI_STREAM;

typedef struct{
	unsigned char R;
	unsigned char G;
	unsigned char B;
}rgb_8;

void myTestPatternGenerator(AXI_STREAM& m_axis_video, ap_uint<16> data[32], ap_uint<32> cntl);
ap_uint<24> set_rgb_8_pixel_value(rgb_8 pixel);

#endif
