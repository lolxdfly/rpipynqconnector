#include "myTestPatternGenerator.h"
#include "utilities.h"

int main (int argc, char** argv) {

	const int FrameCount = 1;

	AXI_STREAM stream;
	ap_uint<16> data[32];
	for(ap_uint<6> i = 0; i < 32; i += 2)
	{
		data[i + 0] = i * 20;
		data[i + 1] = i * 20;
	}
	ap_uint<32> cntl = 0x11;

	ap_uint<24> out_image [HEIGHT][WIDTH];

	for(int frame = 0; frame < FrameCount; frame++){
		myTestPatternGenerator(stream, data, cntl);

		while(stream.size() >= WIDTH * HEIGHT){

			utilities::StreamtoImageArray(stream, out_image);

			char file_name[100];
			snprintf(file_name, sizeof(file_name), "./test_%0*i.bmp",3,frame);
			utilities::writeImage(file_name,out_image);
		}
	}
	return 0;
}
