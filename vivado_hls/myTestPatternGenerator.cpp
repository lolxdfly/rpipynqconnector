#include "myTestPatternGenerator.h"

//Top Level Function
void myTestPatternGenerator(AXI_STREAM& m_axis_video, ap_uint<16> data[32], ap_uint<32> cntl)
{
	#pragma HLS ARRAY_PARTITION variable=data complete dim=1
	#pragma HLS INTERFACE axis register both port=m_axis_video
	#pragma HLS INTERFACE s_axilite port=return
	#pragma HLS INTERFACE s_axilite port=data
	#pragma HLS INTERFACE s_axilite port=cntl

	xf::cv::ap_axiu<24, 1, 1, 1> video;
	rgb_8 pixel;

	//Code for output video generation here
	frame_line_loop: for(ap_uint<12> i = 0; i < HEIGHT; i++){
		frame_pixel_loop: for(ap_uint<12> j = 0; j < WIDTH; j++){
			#pragma HLS PIPELINE rewind

			//*****************************************************************//
			// Set here video.user and video.last for the appropriate pixel    //
			// your code here =>										       //
			video.user = ((i == 0) && (j == 0)); // <==> first pixel in img
			video.last = (j == (WIDTH - 1)); // <==> last pixel in row
			//*****************************************************************//

			//*****************************************************************//
			// Set here the pixel color values in the prepared pixel variable  //
			// your code here =>										       //
			ap_uint<1> hit = false;
			for(ap_uint<6> i2 = 0; i2 < 32; i2 += 2)
			{
				#pragma HLS unroll
				if(!hit)
				{
					hit = -data[i2] >= (i - 1 - HEIGHT) && -data[i2] <= (i + 1 - HEIGHT) &&
							data[i2 + 1] >= (j - 1) && data[i2 + 1] <= (j + 1);
				}
			}
			if(hit)
				pixel.R = 0xFF;
			else
				pixel.R = (cntl & 0xFF);
			pixel.G = (cntl & 0xFF);
			pixel.B = (cntl & 0xFF);
			//*****************************************************************//

			// Assign the pixel value to the data output
			video.data = set_rgb_8_pixel_value(pixel);

			// Send video to stream
			m_axis_video << video;
		}
	}
}

ap_uint<24> set_rgb_8_pixel_value(rgb_8 pixel)
{
	#pragma HLS INLINE

	ap_uint<24> pixel_out;

	pixel_out = (pixel.R << 16) + (pixel.B << 8) + pixel.G;
	return pixel_out;
}
