#ifndef _UTILITIES_H_
#define _UTILITIES_H_

#include "common/xf_axi_sdata.hpp"
#include "ap_int.h"

#include <string>
#include <sstream>
#include <iostream>
#include <fstream>
#include <ios>

namespace utilities
{

template <size_t rows, size_t cols>
int readImage(const char* fileName, ap_uint<24> (&image)[rows][cols]){

	std::ifstream image_file(fileName, std::ios_base::binary | std::ios_base::in);
	if(image_file.is_open()){
		char data;
		//=========================================================//
		// Read BMP-Header
		//=========================================================//
		// (2 Byte) Magic-Number: "BM" 0x42 0x4D
		image_file.get(data);
		if(data == 'B'){
			image_file.get(data);
			if(data != 'M'){
				std::cout << "Only bitmap-files are supported!" << std::endl;
				image_file.close();
				return 1;
			}
		}else{
			std::cout << "Only bitmap-files are supported!" << std::endl;
			image_file.close();
			return 1;
		}

		// (4 Byte) Size of the file: width * height * 3Bpp + 54Byte Header
		image_file.ignore(4);
		// (4 Byte) Reserved: 0
		image_file.ignore(4);
		// (4 Byte) Image Data Offset: 54Bytes
		int data_offset = 0;
		image_file.get(data);
		data_offset += (data << 0) & 0x000000FF;
		image_file.get(data);
		data_offset += (data << 8) & 0x0000FF00;
		image_file.get(data);
		data_offset += (data << 16) & 0x00FF0000;
		image_file.get(data);
		data_offset += (data << 24) & 0xFF000000;

		//=========================================================//
		// Write BMP-Information-Block
		//=========================================================//
		// (4 Byte) Information-Header-Size: 40Bytes
		int info_size = 0;
		image_file.get(data);
		info_size += (data << 0) & 0x000000FF;
		image_file.get(data);
		info_size += (data << 8) & 0x0000FF00;
		image_file.get(data);
		info_size += (data << 16) & 0x00FF0000;
		image_file.get(data);
		info_size += (data << 24) & 0xFF000000;
		// (4 Byte) width of the image: cols
		int width = 0;
		image_file.get(data);
		width += (data << 0) & 0x000000FF;
		image_file.get(data);
		width += (data << 8) & 0x0000FF00;
		image_file.get(data);
		width += (data << 16) & 0x00FF0000;
		image_file.get(data);
		width += (data << 24)  & 0xFF000000;
		// (4 Byte) height of the image: rows (positiv => bottom-up-Bitmap)
		int height = 0;
		bool bottom_up = 1;
		image_file.get(data);
		height += (data << 0) & 0x000000FF;
		image_file.get(data);
		height += (data << 8) & 0x0000FF00;
		image_file.get(data);
		height += (data << 16) & 0x00FF0000;
		image_file.get(data);
		height += (data << 24) & 0xFF000000;
		if(height <  0){
			bottom_up = 0;
			height *= -1;
		}

		if((width != cols) || (height != rows)){
			std::cout << width << "\t|\t" << height << std::endl;
			std::cout << "Dimensions of the image does not match the size of the given array" << std::endl;
			return 1;
		}

		// (2 Byte) not used: 1
		image_file.ignore(2);
		// (2 Byte) bits per pixel: 24
		int bpp = 0;
		image_file.get(data);
		bpp += (data << 0) & 0x000000FF;
		image_file.get(data);
		bpp += (data << 8) & 0x0000FF00;
		if(bpp != 24){
			std::cout << "Only bitmaps with 24bpp are supported" << std::endl;
			return 1;
		}

		// (4 Byte) compression method (no compression): 0
		int compression = 0;
		image_file.get(data);
		compression += (data << 0) & 0x000000FF;
		image_file.get(data);
		compression += (data << 8) & 0x0000FF00;
		image_file.get(data);
		compression += (data << 16) & 0x00FF0000;
		image_file.get(data);
		compression += (data << 24) & 0xFF000000;
		if(compression != 0){
			std::cout << "Only uncompressed bitmaps are supported" << std::endl;
			return 1;
		}

		// (4 Byte) size of image-data in byte: width * height * 3Bpp
		int imageDataSize = 0;
		image_file.get(data);
		imageDataSize += (data << 0) & 0x000000FF;
		image_file.get(data);
		imageDataSize += (data << 8) & 0x0000FF00;
		image_file.get(data);
		imageDataSize += (data << 16) & 0x00FF0000;
		image_file.get(data);
		imageDataSize += (data << 24) & 0xFF000000;

		// (4 Byte) pixel per meter X: 0 (unused for BMP)
		image_file.ignore(4);
		// (4 Byte) pixel per meter Y: 0 (unused for BMP)
		image_file.ignore(4);

		// (4 Byte) Size of Colortable: 0
		int ColortableSize = 0;
		image_file.get(data);
		ColortableSize += (data << 0) & 0x000000FF;
		image_file.get(data);
		ColortableSize += (data << 8) & 0x0000FF00;
		image_file.get(data);
		ColortableSize += (data << 16) & 0x00FF0000;
		image_file.get(data);
		ColortableSize += (data << 24) & 0xFF000000;
		if(ColortableSize != 0){
			std::cout << "Bitmap color tables are not supported" << std::endl;
			return 1;
		}

		// (4 Byte) Used colors of the color table: 0
		int UsedColors = 0;
		image_file.get(data);
		UsedColors += (data << 0) & 0x000000FF;
		image_file.get(data);
		UsedColors += (data << 8) & 0x0000FF00;
		image_file.get(data);
		UsedColors += (data << 16) & 0x00FF0000;
		image_file.get(data);
		UsedColors += (data << 24) & 0xFF000000;
		if(UsedColors != 0){
			std::cout << "Bitmap color tables are not supported" << std::endl;
			return 1;
		}

		//=========================================================//
		// Image Data BGR, 24bpp, bottom-up
		//=========================================================//
		for(int i = 0; i < rows; i++){
			for(int j = 0; j < cols; j++){
				ap_uint<24> Pixel = 0;
				// Blue
				image_file.get(data);
				Pixel += ((data <<  8) & 0x00FF00);
				// Green
				image_file.get(data);
				Pixel += ((data <<  0) & 0x0000FF);
				// Red
				image_file.get(data);
				Pixel += ((data << 16) & 0xFF0000);
				if(bottom_up){
					image[rows-1-i][j] = Pixel;
				}else{
					image[i][j] = Pixel;
				}
			}
		}

		image_file.close();
	}else{
		std::cout << "Error could not read file." << std::endl;
		return 1;
	}

	return 0;
}

template <size_t rows, size_t cols>
void writeImage(const char* fileName, ap_uint<24> (&image)[rows][cols]){

	std::ofstream image_file(fileName, std::ios_base::binary | std::ios_base::out);
	if(image_file.is_open()){

		//=========================================================//
		// Write BMP-Header
		//=========================================================//
		// (2 Byte) Magic-Number: "BM" 0x42 0x4D
		image_file << static_cast<unsigned char>(0x42);
		image_file << static_cast<unsigned char>(0x4D);
		// (4 Byte) Size of the file: width * height * 3Bpp + 54Byte Header
		const int file_size = rows * cols * 3 + 54;
		image_file << static_cast<unsigned char>((file_size >>  0) & 0xFF);
		image_file << static_cast<unsigned char>((file_size >>  8) & 0xFF);
		image_file << static_cast<unsigned char>((file_size >> 16) & 0xFF);
		image_file << static_cast<unsigned char>((file_size >> 24) & 0xFF);
		// (4 Byte) Reserved: 0
		image_file << static_cast<unsigned char>(0);
		image_file << static_cast<unsigned char>(0);
		image_file << static_cast<unsigned char>(0);
		image_file << static_cast<unsigned char>(0);
		// (4 Byte) Image Data Offset: 54Bytes
		const int offset = 54;
		image_file << static_cast<unsigned char>((offset >>  0) & 0xFF);
		image_file << static_cast<unsigned char>((offset >>  8) & 0xFF);
		image_file << static_cast<unsigned char>((offset >> 16) & 0xFF);
		image_file << static_cast<unsigned char>((offset >> 24) & 0xFF);

		//=========================================================//
		// Write BMP-Information-Block
		//=========================================================//
		// (4 Byte) Information-Header-Size: 40Bytes
		const int info_size = 40;
		image_file << static_cast<unsigned char>((info_size >>  0) & 0xFF);
		image_file << static_cast<unsigned char>((info_size >>  8) & 0xFF);
		image_file << static_cast<unsigned char>((info_size >> 16) & 0xFF);
		image_file << static_cast<unsigned char>((info_size >> 24) & 0xFF);
		// (4 Byte) width of the image: cols
		image_file << static_cast<unsigned char>((cols >>  0) & 0xFF);
		image_file << static_cast<unsigned char>((cols >>  8) & 0xFF);
		image_file << static_cast<unsigned char>((cols >> 16) & 0xFF);
		image_file << static_cast<unsigned char>((cols >> 24) & 0xFF);
		// (4 Byte) height of the image: rows (positiv => bottom-up-Bitmap)
		image_file << static_cast<unsigned char>((rows >>  0) & 0xFF);
		image_file << static_cast<unsigned char>((rows >>  8) & 0xFF);
		image_file << static_cast<unsigned char>((rows >> 16) & 0xFF);
		image_file << static_cast<unsigned char>((rows >> 24) & 0xFF);
		// (2 Byte) not used: 1
		image_file << static_cast<unsigned char>((1 >>  0) & 0xFF);
		image_file << static_cast<unsigned char>((1 >>  8) & 0xFF);
		// (2 Byte) bits per pixel: 24
		const int bpp = 24;
		image_file << static_cast<unsigned char>((bpp >>  0) & 0xFF);
		image_file << static_cast<unsigned char>((bpp >>  8) & 0xFF);
		// (4 Byte) compression method (no compression): 0
		image_file << static_cast<unsigned char>((0 >>  0) & 0xFF);
		image_file << static_cast<unsigned char>((0 >>  8) & 0xFF);
		image_file << static_cast<unsigned char>((0 >> 16) & 0xFF);
		image_file << static_cast<unsigned char>((0 >> 24) & 0xFF);
		// (4 Byte) size of image-data in byte: width * height * 3Bpp
		const int image_data_size = rows * cols * 3;
		image_file << static_cast<unsigned char>((image_data_size >>  0) & 0xFF);
		image_file << static_cast<unsigned char>((image_data_size >>  8) & 0xFF);
		image_file << static_cast<unsigned char>((image_data_size >> 16) & 0xFF);
		image_file << static_cast<unsigned char>((image_data_size >> 24) & 0xFF);
		// (4 Byte) pixel per meter X: 0 (unused for BMP)
		image_file << static_cast<unsigned char>((0 >>  0) & 0xFF);
		image_file << static_cast<unsigned char>((0 >>  8) & 0xFF);
		image_file << static_cast<unsigned char>((0 >> 16) & 0xFF);
		image_file << static_cast<unsigned char>((0 >> 24) & 0xFF);
		// (4 Byte) pixel per meter Y: 0 (unused for BMP)
		image_file << static_cast<unsigned char>((0 >>  0) & 0xFF);
		image_file << static_cast<unsigned char>((0 >>  8) & 0xFF);
		image_file << static_cast<unsigned char>((0 >> 16) & 0xFF);
		image_file << static_cast<unsigned char>((0 >> 24) & 0xFF);
		// (4 Byte) Size of Colortable: 0
		image_file << static_cast<unsigned char>((0 >>  0) & 0xFF);
		image_file << static_cast<unsigned char>((0 >>  8) & 0xFF);
		image_file << static_cast<unsigned char>((0 >> 16) & 0xFF);
		image_file << static_cast<unsigned char>((0 >> 24) & 0xFF);
		// (4 Byte) Used colors of the color table: 0
		image_file << static_cast<unsigned char>((0 >>  0) & 0xFF);
		image_file << static_cast<unsigned char>((0 >>  8) & 0xFF);
		image_file << static_cast<unsigned char>((0 >> 16) & 0xFF);
		image_file << static_cast<unsigned char>((0 >> 24) & 0xFF);

		//=========================================================//
		// Image Data BGR, 24bpp, bottom-up
		//=========================================================//
		for(int i = (rows-1); i >= 0; i--){
			for(int j = 0; j < cols; j++){
				// Blue
				image_file << static_cast<unsigned char>((image[i][j] >> 8) & 0xFF);
				// Green
				image_file << static_cast<unsigned char>((image[i][j] >> 0) & 0xFF);
				// Red
				image_file << static_cast<unsigned char>((image[i][j] >> 16) & 0xFF);
			}
		}

		image_file.close();
	}else{
		std::cout << "Error could not create file." << std::endl;
	}
}

template <size_t rows, size_t cols>
int StreamtoImageArray(hls::stream<xf::cv::ap_axiu<24, 1, 1, 1> > (&stream), ap_uint<24> (&image)[rows][cols]){

	for(int i = 0; i < rows; i++){
		for(int j = 0; j < cols; j++){
			xf::cv::ap_axiu<24, 1, 1, 1> Pixel;
			stream >> Pixel;

			// Testing timing signals
			if((i == 0) && (j == 0)){
				if(Pixel.user != 1){
					std::cout << "ERR: \"Start of Frame\" signal was not set at the start of frame" << std::endl;
					return 1;
				}
			}else{
				if(Pixel.user != 0){
					std::cout << "ERR: \"Start of Frame\" signal was set at a pixel which is not the start of a frame" << std::endl;
					return 1;
				}
			}
			if(j == (cols-1)){
				if(Pixel.last != 1){
					std::cout << "ERR: \"End of line\" signal was not set at the end of a line" << std::endl;
					return 1;
				}
			}else{
				if(Pixel.last != 0){
					std::cout << "ERR: \"End of line\" signal was set at a pixel which is not the end of a line" << std::endl;
					return 1;
				}
			}
			image[i][j] = Pixel.data;
		}
	}
	return 0;
}

template <size_t rows, size_t cols>
void ImageArraytoStream(ap_uint<24> (&image)[rows][cols], hls::stream<xf::cv::ap_axiu<24, 1, 1, 1> > (&stream)){
	for(int i = 0; i < rows; i++){
		for(int j = 0; j < cols; j++){
			xf::cv::ap_axiu<24, 1, 1, 1> Pixel;
			if((i == 0) && (j == 0)) Pixel.user = 1;
			else Pixel.user = 0;

			if(j == (cols-1)) Pixel.last = 1;
			else Pixel.last = 0;

			Pixel.data = image[i][j];
			stream << Pixel;
		}
	}
}

}

#endif
