#--------------------Physical Constraints-----------------

set_property BOARD_PIN {raspberry_pi_tri_i_0} [get_ports gpio_io_t[0]]

set_property BOARD_PIN {raspberry_pi_tri_i_1} [get_ports gpio_io_t[1]]

set_property BOARD_PIN {raspberry_pi_tri_i_2} [get_ports gpio_io_t[2]]

set_property BOARD_PIN {raspberry_pi_tri_i_3} [get_ports gpio_io_t[3]]

set_property BOARD_PIN {raspberry_pi_tri_i_4} [get_ports gpio_io_t[4]]

set_property BOARD_PIN {raspberry_pi_tri_i_5} [get_ports gpio_io_t[5]]

set_property BOARD_PIN {raspberry_pi_tri_i_6} [get_ports gpio_io_t[6]]

set_property BOARD_PIN {raspberry_pi_tri_i_7} [get_ports gpio_io_t[7]]

set_property BOARD_PIN {raspberry_pi_tri_i_8} [get_ports gpio_io_t[8]]

set_property BOARD_PIN {raspberry_pi_tri_i_9} [get_ports gpio_io_t[9]]

set_property BOARD_PIN {raspberry_pi_tri_i_10} [get_ports gpio_io_t[10]]

set_property BOARD_PIN {raspberry_pi_tri_i_11} [get_ports gpio_io_t[11]]

set_property BOARD_PIN {raspberry_pi_tri_i_12} [get_ports gpio_io_t[12]]

set_property BOARD_PIN {raspberry_pi_tri_i_13} [get_ports gpio_io_t[13]]

set_property BOARD_PIN {raspberry_pi_tri_i_14} [get_ports gpio_io_t[14]]

set_property BOARD_PIN {raspberry_pi_tri_i_15} [get_ports gpio_io_t[15]]

set_property BOARD_PIN {raspberry_pi_tri_i_16} [get_ports gpio_io_t[16]]

set_property BOARD_PIN {raspberry_pi_tri_i_17} [get_ports gpio_io_t[17]]

set_property BOARD_PIN {raspberry_pi_tri_i_18} [get_ports gpio_io_t[18]]

set_property BOARD_PIN {raspberry_pi_tri_i_19} [get_ports gpio_io_t[19]]

set_property BOARD_PIN {raspberry_pi_tri_i_20} [get_ports gpio_io_t[20]]

set_property BOARD_PIN {raspberry_pi_tri_i_21} [get_ports gpio_io_t[21]]

set_property BOARD_PIN {raspberry_pi_tri_i_22} [get_ports gpio_io_t[22]]

set_property BOARD_PIN {raspberry_pi_tri_i_23} [get_ports gpio_io_t[23]]

set_property BOARD_PIN {raspberry_pi_tri_i_24} [get_ports gpio_io_t[24]]

